package com.apitest.controller;

import com.apitest.dto.PriceGetRequest;
import com.apitest.dto.PriceGetResponse;
import com.apitest.exception.PriceNotFoundException;
import com.apitest.service.PriceServices;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1")
@Validated
public class PriceController {

    private PriceServices priceServices;

    public PriceController(PriceServices priceServices) {
        this.priceServices = priceServices;
    }

    @GetMapping(path = "/pricerange", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<PriceGetResponse> getPriceRange(@Valid @NotNull PriceGetRequest request) throws PriceNotFoundException {
        return new ResponseEntity<>(priceServices.getPriceRange(request), HttpStatus.OK);
    }


}
