package com.apitest.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class PriceGetRequest {

    @NotNull(message = "priceDate is mandatory")
    @DateTimeFormat(pattern = "yyyy-MM-dd-HH.mm.ss")
    private Date date;

    @NotNull(message = "productId is mandatory")
    private Long idProduct;

    @NotNull(message = "brandId is mandatory")
    private Long idBrand;

}
