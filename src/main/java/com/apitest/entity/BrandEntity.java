package com.apitest.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BRANDS")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BrandEntity {

    @Id
    @Column(name = "BRAND_ID", nullable = false)
    private Long idBrand;

    @Column(name = "BRAND_NAME", nullable = false)
    private String name;
}
