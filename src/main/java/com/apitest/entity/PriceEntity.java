package com.apitest.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "PRICES")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PriceEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="BRAND_ID", nullable = false,
            referencedColumnName = "BRAND_ID")
    private BrandEntity idBrand;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="PRODUCT_ID", nullable = false)
    private ProductEntity idProduct;

    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "END_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @Id
    @Column(name = "PRICE_LIST", nullable = false)
    private Long priceList;

    @Column(name = "PRIORITY", nullable = false)
    private Long priority;

    @Column(name = "PRICE", nullable = false)
    private Float price;

    @Column(name = "CURR", nullable = false)
    private String currency;
}
