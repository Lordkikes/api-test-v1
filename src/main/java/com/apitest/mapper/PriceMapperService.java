package com.apitest.mapper;

import com.apitest.dto.PriceGetResponse;
import com.apitest.entity.PriceEntity;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;

//@Mapper(componentModel = "spring")

@Mapper(
		componentModel = "spring",
		collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED,
		nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PriceMapperService {
	
	@Mapping(source = "price.idProduct.idProduct", target = "idProduct")
	@Mapping(source = "price.idBrand.idBrand", target = "idBrand")
	@Mapping(source = "price.priceList", target = "priceList")
	@Mapping(source = "price.startDate", target = "startDate", dateFormat ="yyyy-MM-dd-HH.mm.ss" )
	@Mapping(source = "price.endDate", target = "endDate", dateFormat ="yyyy-MM-dd-HH.mm.ss")
	@Mapping(source = "price.price", target = "price", numberFormat = "##.00")
	PriceGetResponse priceToPriceGetResponseDTO(PriceEntity price);

}
