package com.apitest.repository;

import com.apitest.entity.PriceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface PriceRepository extends JpaRepository<PriceEntity, Long> {

    @Query("Select p From PriceEntity p where p.idProduct.idProduct = :idProduct and p.idBrand.idBrand = :idBrand and  :date between p.startDate and p.endDate order by p.priority desc")
    List<PriceEntity> findByIdProductIdBrand(Long idProduct, Long idBrand, Date date);
}
