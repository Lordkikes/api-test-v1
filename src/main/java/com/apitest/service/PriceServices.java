package com.apitest.service;

import com.apitest.dto.PriceGetRequest;
import com.apitest.dto.PriceGetResponse;
import com.apitest.exception.PriceNotFoundException;

public interface PriceServices {

    PriceGetResponse getPriceRange(PriceGetRequest request) throws PriceNotFoundException;
}
