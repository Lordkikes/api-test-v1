package com.apitest.service.impl;

import com.apitest.dto.PriceGetRequest;
import com.apitest.dto.PriceGetResponse;
import com.apitest.entity.PriceEntity;
import com.apitest.exception.PriceNotFoundException;
import com.apitest.mapper.PriceMapperService;
import com.apitest.repository.PriceRepository;
import com.apitest.service.PriceServices;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.String.format;

@Service
@Log4j2
//@AllArgsConstructor
//@NoArgsConstructor
public class PriceServiceImpl implements PriceServices {

    //@Autowired
    private PriceRepository priceRepository;

    //@Autowired
    private PriceMapperService priceMapperService;

    public PriceServiceImpl(PriceRepository priceRepository, PriceMapperService priceMapperService) {
        this.priceRepository = priceRepository;
        this.priceMapperService = priceMapperService;
    }

    @Override
    public PriceGetResponse getPriceRange(PriceGetRequest request) throws PriceNotFoundException {
        log.info(format("---> getPriceRange for date: %tD and idProduct: %d and idBrand:%d", request.getDate(), request.getIdProduct(),
                request.getIdBrand()));

        List<PriceEntity> findListPrice = priceRepository.findByIdProductIdBrand(request.getIdProduct(), request.getIdBrand(), request.getDate());

        if (findListPrice.isEmpty()) {
            throw new PriceNotFoundException(request.getDate(), request.getIdProduct(), request.getIdBrand());
        }

        PriceGetResponse responseDTO = priceMapperService.priceToPriceGetResponseDTO(findListPrice.get(0));

        return responseDTO;
    }
}
